package lectureNotes;

public class ClassScope {
	
	void scopeMethod1( int var1 ) {
		String var2;
		if( var1 > 0 ) {
			var2 = "above 0";					// this var2 refers to the var2 defined in this method
		} else {
			var2 = "less than or equal to 0";	// this one, too...
		}
		System.out.println( var2 );
	}											// at this point, var2 is gone (out of scope)

	private int var3;							// we did method scope last time, this is CLASS scope
												// it's not static, it's a field like other we've defined
	
	void scopeMethod2( int var1 ) {
		var3 = var1;							// this var3 refers to the class scope definition 
		String var2;
		if( var1 > 0 ) {
			var2 = "above 0";					// this var2 refers to the var2 defined in this method
		} else {
			var2 = "less than or equal to 0";	// this one, too...
		}
		System.out.println( var2 );
		System.out.println( var3 );

	}											// var2 is gone (out of scope), what about var3??
	
	
	/// back to our baby-themed references....
	public void TestTheseScopes() {
		//System.out.println( Baby.servings );		// this would be a static field - does that exist?

		Baby thisBaby = new Baby();
		thisBaby.servings = 3;						// accesses the class scoped field for thisBaby
		System.out.println( thisBaby.servings );	// shouldn't be an issue...  '3'
		
		thisBaby.badFeed( 5 );						// ok - what's thisBaby.servings now??
		System.out.println( thisBaby.servings );	// 3 or 8 or something else??

		thisBaby.goodFeed( 5 );						// ok - what's thisBaby.servings now??
		System.out.println( thisBaby.servings );	// 3 or 8 or something else??

	}
	public class Baby {
		int servings;
		
		void badFeed( int servings ) {				// very confusing - defines a METHOD scope variable servings
			servings = servings + servings;		// what do you think this updates??
		}										// what happens to "method" servings??  "class" servings?? 
		void goodFeed( int servings ) {				// very confusing - defines a METHOD scope variable servings
			this.servings = this.servings + servings;		// what do you think this updates??
		}										// what happens to "method" servings??  "class" servings?? 

		void poop() {
			System.out.println( "All better!");
			servings = 0;						// how many servings are in scope?  what does this do?
		}
	}
}
