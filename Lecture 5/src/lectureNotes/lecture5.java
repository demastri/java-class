package lectureNotes;


public class lecture5 {
	int dog;


	public static void scopeReview() {
		Counter counter1 = new Counter();
		Counter counter2 = new Counter();
		
		counter1.increment();
		counter1.increment();
		counter2.increment();

		System.out.println( "Counter 1:" + counter1.myCount + " " + counter1.ourCount);
		System.out.println( "Counter 2:" + counter2.myCount + " " + counter2.ourCount);
	
	}

	public static void accessReview() {
		BadCreditCard();
		GoodCreditCard();
	}
	
	private static void classScopeNotes() {
		ClassScope scope = new ClassScope();
		
		scope.scopeMethod1(10);
		scope.scopeMethod1(-2);

		scope.scopeMethod2(10);
		scope.scopeMethod2(-2);

		scope.TestTheseScopes();		
	}
	
	
	public static void BadCreditCard() {
		BadCreditCard bcc = new BadCreditCard();
		
		bcc.expenses = 0;
		System.out.println( bcc.cardNumber );
	}
	public static void GoodCreditCard() {
		GoodCreditCard gcc = new GoodCreditCard();
		
		//gcc.expenses = 0;
		//System.out.println( gcc.cardNumber );
	}
	
	public static void main(String[] arguments) {
		scopeReview();
		accessReview();
		classScopeNotes();
	}


}
