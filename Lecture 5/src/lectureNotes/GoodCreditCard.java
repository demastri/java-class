package lectureNotes;

public class GoodCreditCard {
	private String cardNumber = "xyz";
	private double expenses = 0;

	public void charge( double amount ) {
		expenses += amount;
	}
	public String getCardNumber(String password) {
		if( password.equals("SECRET!3*!")) {
			return cardNumber;
		}
		return "jerkface";
	}
	/// the INTERFACE is the thing you want the people who use your code to use.
	/// in this case it's the public methods:
	///   public void charge( double amount )l
	///   public String getCardNumber(String password)l
	
	/// the IMPLEMENTATION is the thing the people who use your code shouldn't care about
	/// in this case it's the private fields:
	///   private String cardNumber = "xyz";
	///   private double expenses = 0;

	/// it's not always public methods / private fields.  Lots of times you'll 
	/// have methods the user shouldn't know or fields you can let users access
	/// Class design is all about figuring out:
	///   1) what you want to do - this will define the PUBLIC interface (fields/methods)
	///        you can write this by writing a tester/driver program before you even start your class
	///        this actually is pretty good practice, called Test Driven Development
	///   2) once you know what users want, anything that you need internally to do it
	///        can be private - hey, they didn't need it for the interface, why expose it?
	/// In this case, the cardNumber's kinda available.  Only supposed to be visible if
	/// they can give you a password - field is private, accessing method public...make sense?? 

}
