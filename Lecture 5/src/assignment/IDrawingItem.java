package assignment;

import java.awt.Graphics;

public interface IDrawingItem {
    public void draw(Graphics surface);
    public void setMovementVector(int xIncrement, int yIncrement);
    public void setSpinRate(int spin);
}
