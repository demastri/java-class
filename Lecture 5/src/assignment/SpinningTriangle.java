package assignment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.sun.org.apache.bcel.internal.classfile.JavaClass;

public class SpinningTriangle implements IDrawingItem {
    int x;
    int y;
    Color color;
    int xDirection = 0;
    int yDirection = 0;
    int spinRate = 0;
    int curAngle = 0;
    
    final int SIZE = 20;

    /**
     * Initialize a new box with its center located at (startX, startY), filled
     * with startColor.
     */
    public SpinningTriangle(int startX, int startY, Color startColor) {
        x = startX;
        y = startY;
        color = startColor;
    }

    /** Draws the box at its current position on to surface. */
    public void draw(Graphics surface) {
        // Draw the object
        int[] px = new int[3];
        int[] py = new int[3];
        
        // in reality, we should think of a triangle with "r" of SIZE/2 centered
        // at 0,0.  Offset it for the current spin angle and current position
        // 0 degrees = straight up (and 120 and 240...)
        
        px[0] = (int)(x + SIZE/2.0*Math.cos( Math.PI/180.0 * curAngle));
        px[1] = (int)(x + SIZE/2.0*Math.cos( Math.PI/180.0 * (curAngle+120)));
        px[2] = (int)(x + SIZE/2.0*Math.cos( Math.PI/180.0 * (curAngle+240)));
        
        py[0] = (int)(y + SIZE/2.0*Math.sin( Math.PI/180.0 * curAngle));
        py[1] = (int)(y + SIZE/2.0*Math.sin( Math.PI/180.0 * (curAngle+120)));
        py[2] = (int)(y + SIZE/2.0*Math.sin( Math.PI/180.0 * (curAngle+240)));
        
        surface.setColor(color);
        surface.fillPolygon(px, py, 3);
        surface.setColor(Color.BLACK);
        ((Graphics2D) surface).setStroke(new BasicStroke(3.0f));
        surface.drawPolygon(px, py, 3);
        
        // Move the center of the object each time we draw it
        x += xDirection;
        y += yDirection;
        curAngle = (curAngle+spinRate)%360;

        // If we have hit the edge and are moving in the wrong direction, reverse direction
        // We check the direction because if a box is placed near the wall, we would get "stuck"
        // rather than moving in the right direction
        if ((x - SIZE/2 <= 0 && xDirection < 0) ||
                (x + SIZE/2 >= SimpleDraw.WIDTH && xDirection > 0)) {
            xDirection = -xDirection;
        }
        if ((y - SIZE/2 <= 0 && yDirection < 0) ||
                (y + SIZE/2 >= SimpleDraw.HEIGHT && yDirection > 0)) {
            yDirection = -yDirection;
        }
    }

    public void setMovementVector(int xIncrement, int yIncrement) {
        xDirection = xIncrement;
        yDirection = yIncrement;
    }

    public void setSpinRate(int spinIncrement) {
        spinRate = spinIncrement;
    }
} 