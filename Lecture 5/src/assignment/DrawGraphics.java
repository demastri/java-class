package assignment;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class DrawGraphics {
	ArrayList<IDrawingItem> items;
	ArrayList<BouncingBox> boxen;
	ArrayList<SpinningTriangle> trips;
    
    /** Initializes this class for drawing. */
    public DrawGraphics() {
    	boxen = new ArrayList<BouncingBox>();
    	trips = new ArrayList<SpinningTriangle>();
        boxen.add(new BouncingBox(200, 50, Color.RED));
        boxen.add(new BouncingBox(50, 150, Color.CYAN));
        boxen.add(new BouncingBox(125, 250, Color.GRAY));
        boxen.get(0).setMovementVector(1,3);
        boxen.get(1).setMovementVector(1,-3);
        boxen.get(2).setMovementVector(2,-1);

        trips.add(new SpinningTriangle(125, 250, Color.ORANGE));
        trips.get(0).setMovementVector(5,-2);
        trips.get(0).setSpinRate(-5);

        items = new ArrayList<IDrawingItem>();
        items.add(new BouncingBox(200, 50, Color.RED));
        items.add(new BouncingBox(50, 150, Color.CYAN));
        items.add(new BouncingBox(125, 250, Color.GRAY));
        items.add(new SpinningTriangle(125, 250, Color.ORANGE));
        items.get(0).setMovementVector(1,3);
        items.get(1).setMovementVector(1,-3);
        items.get(2).setMovementVector(2,-1);
        items.get(3).setMovementVector(5,-2);
        items.get(3).setSpinRate(-5);
    }

    /** Draw the contents of the window on surface. Called 20 times per second. */
    public void draw(Graphics surface) {
        surface.drawLine(50, 50, 250, 250);
        //for( BouncingBox box : boxen )
        //	box.draw(surface);
        //for( SpinningTriangle trip : trips)
        //	trip.draw(surface);

        for( IDrawingItem i : items )
        	i.draw(surface);
        
        surface.drawArc(100, 150, 50, 50, 0, 75);
        surface.drawOval(75, 200, 150, 75);
        surface.drawRoundRect(25, 25, 150, 100, 20, 20);

    }
} 