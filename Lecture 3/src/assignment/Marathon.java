package assignment;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

class Marathon {
	public static void main(String[] arguments) {

		boolean printArrays = false; // flag to use during code testing

		String[] names = { "Elena", "Thomas", "Hamilton", "Suzie", "Phil",
				"Matt", "Alex", "Emma", "John", "James", "Jane", "Emily",
				"Daniel", "Neda", "Aaron", "Kate" };

		int[] times = { 341, 273, 278, 329, 445, 402, 388, 275, 243, 334, 412,
				393, 299, 343, 317, 265 };

		for (int i = 0; printArrays && i < names.length; i++) {
			System.out.println(names[i] + ": " + times[i]);
		}
		
		// original code stub ended here - the printArrays flag was also added
		// almost all of the actual assignment coding is below
		
		int minIndex = IndexOfSmallest(times);
		if (minIndex >= 0)
			System.out.println("The fastest time was by " + names[minIndex]
					+ ": " + times[minIndex]);
		else
			System.out.println("There was no minimum value returned");
		
		if( true )
			return;
		
		int secondMinIndex = IndexOfSecondSmallest(times);
		if (secondMinIndex >= 0)
			System.out.println("The second fastest time was by "
					+ names[secondMinIndex] + ": " + times[secondMinIndex]);
		else
			System.out.println("There was no second minimum value returned");

		for (int i = 0; printArrays && i < names.length; i++) {
			System.out.println(names[i] + ": " + times[i]);
		}
		System.out.println("\nUsing nth smallest: from the bottom:");
		
		for (int i = 0; i < 5 && i < names.length; i++) {
			int ii = IndexOfNthSmallest(times, i); 
			System.out.println(names[ii] + ": " + times[ii]);
		}
	}

	public static int IndexOfSmallest(int[] values) {
		if (values.length == 0)
			return -1;
		int curMinIndex = 0;
		for (int i = 1; i < values.length; i++) {
			if (values[i] < values[curMinIndex]) {
				curMinIndex = i;
			}
		}
		return curMinIndex;
	}

	public static int IndexOfSecondSmallest(int[] values) {
		int indexOfSmallest;
		if (values.length < 2 || (indexOfSmallest = IndexOfSmallest(values)) == -1 )
			return -1;
		// ok, we know where the smallest value is, we need to make it the biggest... :)
		int[] myValues = values.clone();
		myValues[indexOfSmallest] = Integer.MAX_VALUE;
		return IndexOfSmallest(myValues);
	}

	public static int IndexOfNthSmallest(int[] values, int n) {	// 0 = smallest
		if( values.length < n )
			return -1;
		int[] myValues = values.clone();
		while( n-- > 0 ) {
			int nextLowest = IndexOfSmallest(myValues);
			myValues[nextLowest] = Integer.MAX_VALUE;
		}
		return IndexOfSmallest(myValues);
	}
}
