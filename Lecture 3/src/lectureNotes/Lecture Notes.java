package lectureNotes;

class lectureNotes {
	public static void main(String[] arguments) {

		//learnAboutProgrammingStyle();

		//learnAboutLoops();

		learnAboutArrays(arguments);
	}

	public static void learnAboutProgrammingStyle() {
		String a1;
		int a2;
		double b; // BAD!!
		
		String firstName; // GOOD
		String lastName; // GOOD
		int temperature; // GOOD

		int addend1 = 12;
		int addend2 = 41;
		int sum = addend1 * addend2;	// Is this bad?  Why/why not?
		// don't just use mnemonic names, have them actually do what the user would expect

		// use indentation to VISUALLY SHOW SCOPE and focus
		// quickly - what does this do?
int x = 5;x = x * x;if (x > 20) { System.out.println(x + " is greater than 20.");	} double y = 3.4;
		// now, quickly - what does this do?
		int xx = 5;
		xx = xx * xx;
		if (xx > 20) { 
			System.out.println(xx + " is greater than 20.");	
		} 
		double yy = 3.4;
		
		// as above, whitespace in lines, like indents
		// and just like in regular text is a good thing
		double fahr=40.0;
		// BAD!!
		double cel1=fahr*42.0/(13.0-7.0);
		// GOOD
		double cel2 = fahr * 42.0 / (13.0 - 7.0);

		// finally, blank spaces are additional whitespace that helps readability
		int xxx = 5;
		xxx = xxx * xxx;

		if (xxx > 20) {
			System.out.println(xxx + " is > 20."); 
		}
		
		double yyy = 3.4;

		// finally, don't duplicate tests
		double basePay = 10.0;
		int hours = 50;
		if (basePay < 8.0) { 
			// do something
		} else if (hours > 60) { 
			// else do some other thing
		} else if (basePay >= 8.0 && hours <= 60){ 
			// do the default case ... is it possible for this test to EVER fail??
		}
		// redundant, so don't do it...
		if (basePay < 8.0) { 
			// do something
		} else if (hours > 60) { 
			// else do some other thing
		} else {
			// do the default case ... fully equivalent to the prior code
		}
	}

	public static void learnAboutLoops() {
		// let's start with a while loop
		//int ii= 0;			// initialization
		//while(ii < 200 ) { 		// test
		//	System.out.println("Rule #" + (ii+0));	// work 
		//	ii = ii+1;		// increment
		//}
		// let's continue with a for loop
		// NOTE that initialization, test and increment are all done in one statement		
		//for( int i= 0; i < 3; i++ ) {	// the increment could be i = i+1 or i += 1 		
		//	System.out.println("Rule #" + i);	// work 
		//}
		
		// the break statement is used to "break" out of a loop outside of the test
		//for( int i= 0; i < 100; i++ ) {
		//	if( i == 50 )
		//		break;	// control continues AFTER the close of the loop scope
		//	System.out.println("Rule #" + i);	// work 
		//}
		
		// the continue statement is used to end this iteration, and go back to the top of the scope
		//for( int i= 0; i < 100; i++ ) {
		//	if( i == 50 )
		//		continue;	// control returns to the increment and test in a for loop 
		//	System.out.println("Rule #" + i);	// work 
		//}

		// embedded loops
		for (int i = 0; i < 3; i++) { 
			for (int j = 2; j < 4; j++) { 
				System.out.println (i + " " + j); 
			} 
			// note that 'j' here is undefined!!!
		}
	}

	public static void learnAboutArrays(String[] arguments) {
		int[] values = new int[5];
		values[0] = 12; // CORRECT
		values[4] = 12; // CORRECT
		//values[5] = 12;	// correct???
		
		
		//int[] valueArray = {1, 2.5, 3, 3.5, 4};


		//int[] v1 = {1,2,3,4,5};
		
		int[] v2 = new int[5];
		v2[0] = 1;
		v2[1] = 2;
		v2[2] = 3;
		v2[3] = 4;
		v2[4] = 5;
		
		System.out.println( arguments.length );
		for( int i=0; i<arguments.length; i++ )
			System.out.println( arguments[i] );
		
		int[] MoreValues = new int[5];
		for (int i=0; i<MoreValues.length; i++) { 
			MoreValues[i] = i; 
			int y = MoreValues[i] * MoreValues[i]; 
			System.out.println(MoreValues[i] + ", " + y);
		}
		System.out.println(MoreValues);	// don't do this :)
	}
}