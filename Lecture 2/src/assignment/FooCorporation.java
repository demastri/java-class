package assignment;
class FooCorporation {
	public static void main (String[] arguments) {
		CalculatePay( 7.50, 35 );
		CalculatePay( 8.20, 47 );
		CalculatePay( 10.00, 73 );
	}

	private static void CalculatePay( double payRate, int hours) {
		// it's always a good idea to define given values as variables - they will change and your code is easier to read
		int maxHours = 60;
		int maxHoursAtNormalRate = 40;
		double overtimeRateMultiplier = 1.5;
		double overtimePayRate = overtimeRateMultiplier * payRate;
		double minRate = 8.00;
		int errors = 0;
		
		if( payRate < minRate )
		{
			System.out.println( "Pay ("+payRate+") is less than the minimum allowed value of $"+minRate+"/hour");
			errors++;
		}
		if( hours > maxHours )
		{
			System.out.println( "Hours ("+hours+") is greater than the maximum allowed value of "+maxHours+" hours");
			errors++;
		}
		if( errors == 0 )
		{
			double pay;
			// method 1
			int normalHours = hours;
			int overtimeHours = 0;
			
			if( hours > maxHoursAtNormalRate )
			{
				normalHours = maxHoursAtNormalRate;
				overtimeHours = (hours-maxHoursAtNormalRate);
			}
			pay = payRate * normalHours + overtimePayRate * overtimeHours;
			System.out.println( " method 1 - Pay earned for "+hours+" Hours at $"+payRate+"/hour is $" + pay );
			
			// method 2
			pay = payRate * hours;
			if( hours > maxHoursAtNormalRate ) {
				// 2a 
				pay += (overtimeRateMultiplier-1) * payRate * (hours-maxHoursAtNormalRate);	// why overtimeRate-1??
				System.out.println( " method 2a - Pay earned for "+hours+" Hours at $"+payRate+"/hour is $" + pay );
				// or 2b 
				pay = payRate * maxHoursAtNormalRate + overtimePayRate * (hours-maxHoursAtNormalRate);
				System.out.println( " method 2b - Pay earned for "+hours+" Hours at $"+payRate+"/hour is $" + pay );
				// or 2c
				pay = payRate * (maxHoursAtNormalRate + overtimeRateMultiplier * (hours-maxHoursAtNormalRate));
				System.out.println( " method 2c - Pay earned for "+hours+" Hours at $"+payRate+"/hour is $" + pay );
			}

			// print result...
			System.out.println( "Pay earned for "+hours+" Hours at $"+payRate+"/hour is $" + pay );
		}
	}
}