package lectureNotes;
class lectureNotes {
	public static void main( String[] arguments) {
		//doDivision();
		//String five = 5;
		//doCasts();

		//System.out.println("Line 1"); 
		//threeLines(); 
		//System.out.println("Line 2");

		//int value = 2; 
		//printSquare(value); 
		//printSquare(3); 
		//printSquare(value*2);

		//printSquare((int)"hello");
		//printSquare(5);

		//times( 2, 2 );
		//times( 3, 4 );

		//System.out.println( square(5) );
		//System.out.println( square(2) );
		
		//int x = 5;
		//System.out.println( "Main x = "+x );
		//printSquareScope( x ); 
		//System.out.println( "Main x = "+x );
		
		//test1( 6 );
		//test1( 5 );
		//test1( 4 );

		//test2( 6 );
		//test2( 5 );
		//test2( 4 );

		//test3( 6 );
		//test3( 5 );
		//test3( 4 );

		test4();
	}

	public static void doDivision () {
		System.out.println( "Division Checks" );
		System.out.println( 5.0/2.0 );
		System.out.println( 4/2 );
		System.out.println( 5/2 );
		System.out.println( (double)(5/2) );
	}
	public static void doCasts() {
		int a = 2; // a = 2
		double b = 2; // a = 2.0 (Implicit)
		int c = 0;
		//c = 18.7; // ERROR
		int d = (int)18.7; // a = 18
		double e = 2/3; // a = 0.0
		double f = (double)2/3; // a = 0.6666

		System.out.println( "Cast Checks" );
		System.out.println( a+" "+b+" "+c+" "+d+" "+e+" "+f );
	}
	public static void newLine() {
		System.out.println("");
	}
	public static void threeLines() {
		newLine(); newLine(); newLine();
	}
	
	//public static void printSquare(int x){ 
	//	System.out.println(x*x); 
	//}
	public static void printSquare(double x){ 
		System.out.println(x*x); 
	}
	
	public static void times (double a, double b){ 
		System.out.println(a * b); 
	}
	
	public static double square(double x){ 
		return x*x; 
	}
	public static void printSquareScope(int x){ 
		System.out.println( "printSquare x = "+x );
		x = x*x;
		System.out.println( "printSquare x = "+x );
	}
	public static void checkScope(){ 
		int x = 5; 
		if (x == 5){
	//		int x = 6;
			int y = 72;
			System.out.println("x = " + x + " y = " + y);
		}
		//System.out.println("x = " + x + " y = " + y); 
	}
	public static void test1(int x){ 
		if (x > 5){ 
			System.out.println(x + " is > 5"); 
		} 
	}
	public static void test2(int x){ 
		if (x > 5){ 
			System.out.println(x + " is > 5"); 
		} else { 
			System.out.println(x + " is not > 5"); 
		} 
	}

	public static void test3(int x){ 
		if (x > 5){ 
			System.out.println(x + " is > 5"); 
		} else if (x == 5){ 
			System.out.println(x + " equals 5"); 
		} else { 
			System.out.println(x + " is < 5"); 
		} 
	}
	public static void test4(){ 
		double a = Math.cos( Math.PI / 2);
		double b = 0.0;
		if( a == b )
			System.out.println("Cos 90 = 0, like we expect"); 
		else
			System.out.println("Cos 90 <> 0 ???"); 
	}
}

