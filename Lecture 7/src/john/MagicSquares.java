package john;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class MagicSquares {

	int referenceSum;
	int squareSize;
	ArrayList<ArrayList<Integer>> numbers = new ArrayList<ArrayList<Integer>>();
	int numRows;
	
	
	public static void main(String args[]) {
		
		String filename1 = "./src/assignment/Mercury.txt";
		String filename2 = "./src/assignment/Luna.txt";
		ArrayList<String> filenames = new ArrayList<String>();
		filenames.add(filename1);
		filenames.add(filename2);
		filenames.add("./src/assignment/Bad Sum.txt");
		filenames.add("./src/assignment/BadData.txt");
		filenames.add("./src/assignment/Ragged.txt");
		filenames.add("./src/assignment/Ragged2.txt");
		filenames.add("./src/assignment/Ragged3.txt");
		filenames.add("./src/assignment/Ragged4.txt");

		
		for (String f : filenames) {
			MagicSquares ms = new MagicSquares();
			boolean Loaded = ms.Load(f) && ms.Initialize() && ms.isValid();
			if (Loaded) {
				if (ms.CheckRows() && ms.CheckColumns() && ms.CheckDiagonals()) {
					System.out.println("Reference Sum: " + ms.referenceSum);
					System.out.println("File " + f + " is a magic square");
				} else {
					System.out.println("File " + f + " is NOT a magic square");
				}
			} else {
				System.out.println("The file you submitted was incorrectly formatted.");
			}
		}
	}

	// Loads the file and converts it into
	public boolean Load(String filename) {
		try {
			System.out.println(filename + ":");
			FileReader fr = new FileReader(filename);
			BufferedReader br = new BufferedReader(fr);
			String line = null;
			while ((line = br.readLine()) != null) {
				// System.out.println(line);
				String[] strings = line.split("\t");
				ArrayList<Integer> ints = new ArrayList<Integer>();
				for (int i = 0; i < strings.length; i++) {
					if (strings.length != 1) {
						ints.add(Integer.valueOf(strings[i]));
					}

				}
				if (ints.size() != 0) {
					numbers.add(ints);
				}
			}
			br.close();
		} catch (FileNotFoundException err) {
			System.out.println("The file you have requested does not exist");
			return false;
		} catch (IOException e) {
			System.out.println("There was an error accessing the file");
			return false;
		} catch (NumberFormatException x) {
			return false;
		}
		return true;
	}

	public boolean Initialize() {
		try {
			ArrayList<Integer> row1 = numbers.get(0);
		
		for (int i = 0; i < row1.size(); i++) {
			referenceSum += row1.get(i);
		}
	}catch (IndexOutOfBoundsException i){
		return false;
	}
		return true;
	}

	public boolean isValid() {
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.size() != numbers.get(i).size()) {
				return false;
			}
		}
		return true;
	}

	public boolean CheckRows() {
		int checkingSum = 0;
		// System.out.println( "Checking Rows: ");
		for (int i = 0; i < numbers.get(0).size(); i++) {
			ArrayList<Integer> row = numbers.get(i);
			for (int x = 0; x < row.size(); x++) {
				// System.out.print(row.get(x) + " ");
				checkingSum += row.get(x);
			}
			// System.out.println(checkingSum);
			if (checkingSum != referenceSum) {
				return false;
			}
			checkingSum = 0;
		}
		// System.out.println("");
		return true;
	}

	public boolean CheckColumns() {
		int checkingSum = 0;
		// System.out.println( "Checking Columns: ");
		int Size = numbers.get(0).size();
		for (int r = 0; r < Size; r++) {
			for (int c = 0; c < Size; c++) {
				// System.out.print(numbers.get(c).get(r) + " ");
				checkingSum += numbers.get(c).get(r);
			}
			// System.out.println(checkingSum);
			if (checkingSum != referenceSum) {
				return false;
			}
			checkingSum = 0;
		}
		// System.out.println("");
		return true;
	}

	public boolean CheckDiagonals() {
		int checkingSum = 0;
		// System.out.println( "Checking Diagonals: ");
		int Size = numbers.get(0).size();
		for (int d = 0; d < Size; d++) {
			// System.out.print(numbers.get(d).get(d) + " ");
			checkingSum += numbers.get(d).get(d);
		}
		if (checkingSum != referenceSum) {
			return false;
		}
		// System.out.println (checkingSum);
		checkingSum = 0;
		for (int i = Size - 1; i >= 0; i--) {
			for (int x = 0; x < Size; x++) {
				if ((x + i) == Size - 1) {
					// System.out.print(numbers.get(i).get(x) + " ");
					checkingSum += numbers.get(i).get(x);
				}
			}
		}
		// System.out.println (checkingSum);
		if (checkingSum != referenceSum) {
			return false;
		}
		// System.out.println("");
		return true;
	}
}