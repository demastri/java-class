package assignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class MagicSquares {
	private String fileLoc; 
	private ArrayList<ArrayList<Integer>> thisSquare;
	private int refCount;
	private int refSize;
	private int ROW_CHECK = 0;
	private int COL_CHECK = 1;
	private int LR_DIAG_CHECK = 2;
	private int RL_DIAG_CHECK = 3;
	
	public MagicSquares(String filePath, String pathName)
	{
		fileLoc = filePath+pathName;
	}
    public boolean isMagic() throws IOException, NumberFormatException {
		return readFile() && checkRows() && checkCols() && checkDiag();
    }

	private boolean readFile() throws IOException, NumberFormatException {
        BufferedReader reader = new BufferedReader(new FileReader(fileLoc));
        thisSquare = new ArrayList<ArrayList<Integer>>();
        refSize = -1;
        try {
	        // For each line in the file ...
	        String line;
	        while ((line = reader.readLine()) != null ) {
		        ArrayList<Integer> thisLine = new ArrayList<Integer>();
	            // ... sum each row of numbers
	            String[] parts = line.split("\t");
	            if( refSize == -1)
	            	refSize = parts.length;
	            else if( refSize != parts.length ) {
	    	        System.out.println( "bad col count..." );
	    	        return false;
	            }
	            for (String part : parts) {
	                thisLine.add( (Integer)Integer.parseInt(part) );
	            }
	            thisSquare.add( thisLine );
	        }
            if( refSize != thisSquare.size() ) {
    	        System.out.println( "bad row count..." );
            	return false;
            }
	        
			refCount = getCount( 0,0 );
	        System.out.println( "sum = " + refCount + " size = " + refSize );
	        return true;
        }
        finally {
            reader.close();
        }
	}
	private int getCount( int type, int nbr )
	{
        int thisSum = 0;
        for( int r=0; r<refSize; r++ ) 
        {
        	int thisRow = (type == ROW_CHECK ? nbr : (type == COL_CHECK ? r : (type == LR_DIAG_CHECK ? r : refSize -1 - r)));
        	int thisCol = (type == ROW_CHECK ? r : (type == COL_CHECK ? nbr : (type == LR_DIAG_CHECK ? r : refSize -1 - r)));
        	thisSum += thisSquare.get(thisRow).get(thisCol);
        }
        return thisSum;
	}
	
	private boolean checkRows()
	{
		for( int r=0; r<refSize; r++ ) 
			if( getCount(ROW_CHECK,r) != refCount ) {
		        System.out.println( "Bad count..." );
				return false;
			}
		return true;
	}
	private boolean checkCols()
	{
		for( int r=0; r<refSize; r++ ) 
			if( getCount(COL_CHECK,r) != refCount ) {
		        System.out.println( "Bad count..." );
				return false;
			}
		return true;
	}
	private boolean checkDiag()
	{
		if( getCount(LR_DIAG_CHECK,-1) != refCount || getCount(RL_DIAG_CHECK,-1) != refCount ) {
	        System.out.println( "Bad count..." );
			return false;
		}
		return true;
	}
	
    public static void main(String[] args) throws IOException {
        String filePath = "./src/assignment/";
    	String[] fileNames = { "Mercury.txt", "Luna.txt", "xxx.xxx", "Bad Sum.txt", "BadData.txt", "Ragged.txt", "Ragged2.txt", "Ragged3.txt", "Ragged4.txt" };
        for (String fileName : fileNames) {
        	MagicSquares s = new MagicSquares(filePath, fileName);
        	try {
                System.out.println(fileName + " is magic? " + s.isMagic());
        	}
        	catch( IOException e)
        	{
                System.out.println("uh, oh...problem opening file <" + fileName + ">");
                //throw(e);
        	}
        	catch( NumberFormatException e)
        	{
                System.out.println("uh, oh...the file contained non-numeric text... <" + fileName + ">");
                //throw(e);
        	}
            System.out.println("");
        }
    }
}
