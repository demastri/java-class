package assignment;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class DrawGraphics {
    ArrayList<Mover> movers;

    /** Initializes this class for drawing. */
    public DrawGraphics() {
    	movers = new ArrayList<Mover>();
    	
        Rectangle box = new Rectangle(50, 150, Color.RED, 100);
        movers .add( new Bouncer(100, 170, box) );
        movers.get(0).setMovementVector(3, 1);

        SpinningTriangle trip = new SpinningTriangle( 15, 45, Color.CYAN, 10);
        movers.add( new Bouncer(75, 100, trip) );
        movers.get(1).setMovementVector(-3, -2);
        
        Rectangle box2 = new Rectangle(40, 15, Color.ORANGE, 0);
        movers.add( new StraightMover(200, 200, box2) );
        movers.get(2).setMovementVector(3, -2);

        SpinningTriangle trip2 = new SpinningTriangle( 40, 40, Color.YELLOW, 20);
        movers.add( new StraightMover(200, 200, trip2) );
        movers.get(3).setMovementVector(-3, -2);
    }

    /** Draw the contents of the window on surface. */
    public void draw(Graphics surface) {
    	for( Mover m : movers )
    		m.draw(surface);
    }
}
