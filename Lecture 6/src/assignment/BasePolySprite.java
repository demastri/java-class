package assignment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;


public class BasePolySprite implements Sprite {
    protected int width;
    protected int height;
    protected Color color;
    protected ArrayList<Integer> vertexAngles;

    int spinRate = 0;
    int curAngle = 0;
    
    public BasePolySprite(int width, int height, Color color, int spinIncrement) {
        this.width = width;
        this.height = height;
        this.color = color;
        spinRate = spinIncrement;
        
        vertexAngles = new ArrayList<Integer>();
    }

	@Override
    /** Draws the box at its current position on to surface. */
    public void draw(Graphics surface, int x, int y) {
        // Draw the object
        int[] px = new int[vertexAngles.size()];
        int[] py = new int[vertexAngles.size()];
        
        // in reality, we should think of a triangle with "r" of SIZE/2 centered
        // at 0,0.  Offset it for the current spin angle and current position
        // 0 degrees = straight up (and 120 and 240...)

        for( int i=0; i<vertexAngles.size(); i++ ) {
            px[i] = (int)(x + width/2.0 + width/2.0*Math.cos( Math.PI/180.0 * (curAngle+vertexAngles.get(i))));
            py[i] = (int)(y + height/2.0 + height/2.0*Math.sin( Math.PI/180.0 * (curAngle+vertexAngles.get(i))));
        }
        
        surface.setColor(color);
        surface.fillPolygon(px, py, vertexAngles.size());
        surface.setColor(Color.BLACK);
        ((Graphics2D) surface).setStroke(new BasicStroke(3.0f));
        surface.drawPolygon(px, py, vertexAngles.size());
        
        curAngle = (curAngle+spinRate)%360;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    
    public void setSpinRate(int spinIncrement) {
        spinRate = spinIncrement;
    }
}
