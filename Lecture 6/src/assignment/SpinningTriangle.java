package assignment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.sun.org.apache.bcel.internal.classfile.JavaClass;

public class SpinningTriangle extends BasePolySprite {
    /**
     * Initialize a new box with its tl located at (startX, startY), filled
     * with startColor.
     */
    public SpinningTriangle(int width, int height, Color color, int spin) {
    	super( width, height, color, spin );
    	
    	vertexAngles.add(0);
    	vertexAngles.add(120);
    	vertexAngles.add(240);
    }


} 