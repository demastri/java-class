package assignment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Rectangle extends BasePolySprite {

    /**
     * Create a box that has dimensions width and height, filled with
     * startColor.
     */
    public Rectangle(int width, int height, Color color, int spin) {
    	super( width, height, color, spin );
    	vertexAngles.add(45);
    	vertexAngles.add(135);
    	vertexAngles.add(225);
    	vertexAngles.add(315);
    }
}
