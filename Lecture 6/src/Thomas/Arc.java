package Thomas;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Arc implements Sprite{
	int x;
	int y;
	private int width;
	private int height;
	private int startAngle;
	private int arcAngle;

	public Arc(int somex, int somey, int somewidth, int someheight,int somestartAngle, int somearcAngle) {
		x = somex;
		y = somey;

		width = somewidth;
		height = someheight;

		startAngle = somestartAngle;
		arcAngle = somearcAngle;


	}

	public void draw(Graphics surface, int x, int y) {
		
		surface.setColor(Color.GREEN);
		surface.fillArc(x, y, width, height, startAngle, arcAngle);
		
		surface.setColor(Color.GREEN);
		((Graphics2D) surface).setStroke(new BasicStroke(3.0f));
		
		surface.setColor(Color.GREEN);
		surface.drawArc(x, y, width, height, startAngle, arcAngle);
	}
	public int getWidth() {

		return width;

	}

	public int getHeight() {
		return height;
	}
}
