package Thomas;

import java.awt.Color;
import java.util.ArrayList;


import java.awt.Graphics;

public class DrawGraphics {
	Bouncer movingSprite;
	Bouncer someKindOfBouncer;
	StraightMover rectangularStraightMover;
	StraightMover arcStraightMover;
	ArrayList <Bouncer> everyKindOfBouncer = new ArrayList <Bouncer>();
	ArrayList <StraightMover> everyKindOfStraightMover = new ArrayList <StraightMover>();
	/** Initializes this class for drawing. */
	
	public DrawGraphics() {
		Rectangle box = new Rectangle(15, 20, Color.RED); // initializes a new rectangle
		Oval elipse = new Oval(40, 85, Color.BLUE); // initializes a new oval
		
		Arc gothic = new Arc(75, 197, 84, 102, 76, 82);
		Rectangle veryboxlike = new Rectangle(30, 50, Color.PINK);
	
		everyKindOfBouncer.add(movingSprite= new Bouncer(100, 170, box)); // Add some moving, bouncing box to an array list	 
		everyKindOfBouncer.add(someKindOfBouncer = new Bouncer(280, 256, elipse)); // Add some moving, bouncing oval to an array list
		
		everyKindOfStraightMover.add(arcStraightMover = new StraightMover(1, 1, gothic)); // Add an arc straight mover to the array list
		everyKindOfStraightMover.add(rectangularStraightMover = new StraightMover(90, 90, veryboxlike));
		
		movingSprite.setMovementVector(3, 6);
		someKindOfBouncer.setMovementVector(2, 3);
		arcStraightMover.setMovementVector(1, 2);
		rectangularStraightMover.setMovementVector(2,1);
	}

	/** Draw the contents of the window on surface. */
	public void draw(Graphics surface) {
		for (Bouncer s : everyKindOfBouncer){
			s.draw(surface);
		}
		
		for (StraightMover y : everyKindOfStraightMover){
			y.draw(surface);
		}
		
	}
	
	
	
	}
	

