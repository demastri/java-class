package lectureNotes;


class lectureNotes {
	static lectureNotes thisObj = new lectureNotes();

	// why am i static???
	public static void main(String[] arguments) {
		
		//thisObj.learnAboutObjectOrientedProgramming();
		
		//thisObj.learnAboutDefiningClasses();
		
		//thisObj.learnAboutUsingClasses();
		
		//thisObj.learnAboutReferencesVsValues();
		
		thisObj.learnAboutStaticTypesAndMethods();
		
	}
	
	public class Nurse {
		String nurseyStuff;
	}
	public class ER {
		int doctorsOnStaff;
	}
	public class Nursery {
		Baby[] babies;
		Nurse[] nurses;
	}
	public class Hospital {
		Nursery nursery;
		ER er;
		
		// Hospital affiliatedSubHospital;	// is this ok???
	}
	
	void learnAboutObjectOrientedProgramming() {
		String babyName1;
		double babyWeight1;
		String babyName2;
		double babyWeight2;
		String babyName3;
		double babyWeight3;
		// ...
		String babyName500;
		double babyWeight500;

		// ... yipes ...
		
		// using classes
		Baby[] babyArray = new Baby[100];		// define the array
		babyArray[0] = new Baby();				// actually create an instance
		Hospital myHospital = new Hospital();	// create a standalone instance of a hospital
	}
	
	public class Point {
		double x;
		double y;
		
		Point() {
			x = y = 0;
		}
		Point( double newX, double newY ) {
			x = newX;
			y = newY;
		}
	}
	
	void learnAboutDefiningClasses() {
		Point p1 = new Point();
		Point p2 = new Point( 3, -4 );
		Point p3 = new Point( 3.14, -4.159 );

		
	}
	
	void learnAboutUsingClasses() {
		Baby shiloh = new Baby("Shiloh Jolie-Pitt", true);
		Baby knox = new Baby("Knox Jolie-Pitt", true);
		
		System.out.println(shiloh.name); 
		System.out.println(shiloh.numPoops);
		
		shiloh.sayHi(); // �Hi, my name is ...� 
		shiloh.eat(1);
	}
	
	void learnAboutReferencesVsValues() {
		Baby shiloh1 = new Baby("shiloh", true); 
		Baby shiloh2 = new Baby("shiloh", true);

		if( shiloh1 == shiloh2 )
			System.out.println("They're the same...surprising");
		else
			System.out.println("No surprise, they're different, stored at different ADDRESSES..."); 
		
		shiloh1.sayHi();
		shiloh1.name = "Some other name";
		shiloh1.sayHi();
		
		shiloh1=shiloh2;
		shiloh1.sayHi();
		shiloh2.name = "updating OTHER baby";
		shiloh1.sayHi();		
		
		int someVar = 0;
		int[] someArray = { 0,1,2 };
		
		doSomething( someVar, someArray, shiloh1 );
		System.out.println("someVar was 0, now = "+someVar);
		System.out.println("someArray[0] was 0, now = "+someArray[0]);
		shiloh1.sayHi();		
		
		
	}
	
	void doSomething(int x, int[] ys, Baby b) { 
		x = 99;
		ys[0] = 99;
		b.name = "99";
	}
	
	void learnAboutStaticTypesAndMethods() {
		Baby shiloh1 = new Baby("shiloh1", true); 
		Baby shiloh2 = new Baby("shiloh2", true);

		System.out.println("total babies made = "+shiloh1.numBabiesNade);
		System.out.println("total babies made = "+shiloh2.numBabiesNade);

		Baby.staticcry( shiloh1 );
		shiloh2.cry();	
	}
}