package lectureNotes;
public class Baby {
	static int numBabiesNade = 0;
	
	// fields on a class - these are the attributes of a class (nouns if you like)
	String name;
	boolean isMale;
	double weight;
	double decibels;
	int numPoops = 0;
	// Baby[] siblings;	// this is actually a pretty terrible idea...explain

	
	// methods on a class - these are the things you DO with a class (verbs if you like)
	Baby() {
		numBabiesNade++;
	}
	Baby( String myName, boolean maleBaby ) {
		weight = 5.0;
		name = myName;
		isMale = maleBaby;
		numBabiesNade++;
	}
	
	void poop() {
		numPoops += 1;
		System.out.println("Dear mother, I have pooped. Ready the diaper.");
	}

	void sayHi() {
		System.out.println( "Hi, my name is " + name );
	}

	void eat(double foodWeight) {
		if (foodWeight >= 0 && foodWeight < weight) {
			weight = weight + foodWeight;
		}
	}
	
	static void staticcry(Baby thebaby) { 
		System.out.println(thebaby.name + "cries"); 
	}
	void cry() { 
		System.out.println(name + "cries"); 
	}
	
	static void whoami() {
		//System.out.println(name);	// why is this an error???
	}
}
