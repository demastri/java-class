package assignment;

public class Library {
	// Add the missing implementation to this class
	static String openingHours = "Libraries are open daily from 9am to 5pm.";
	String address;
	Book[] books;
	int nbrBooks;

	public Library(String libraryAddress) {
		address = libraryAddress;
		books = new Book[100];
		nbrBooks = 0;
	}

	public int addBook(Book newBook) {
		books[nbrBooks++] = newBook;
		return nbrBooks;
	}

	static public void printOpeningHours() {
		System.out.println(openingHours);
	}

	public void printAddress() {
		System.out.println(address);
	}

	public boolean borrowBook(String title) {
		for (int i = 0; i < nbrBooks; i++)
			if (books[i].getTitle() == title)
				if (!books[i].isBorrowed()) {
					books[i].borrowed();
					System.out.println("You successfully borrowed " + title);
					return true;
				} else {
					System.out.println("Sorry, this book is already borrowed.");
					return false;
				}
		System.out.println("Sorry, this book is not in our catalog.");
		return false;
	}

	public boolean returnBook(String title) {
		for (int i = 0; i < nbrBooks; i++)
			if (books[i].getTitle() == title)
				if (books[i].isBorrowed()) {
					books[i].returned();
					System.out.println("You successfully returned " + title);
					return true;
				} else {
					System.out.println("Sorry, this book is not borrowed.");
					return false;
				}
		System.out.println("Sorry, this book is not in our catalog.");
		return false;
	}
	
	public void printAvailableBooks() {
		if( nbrBooks == 0 )
			System.out.println("No book in catalog");
			
		for (int i = 0; i < nbrBooks; i++)
			if (!books[i].isBorrowed()) {
				System.out.println(books[i].getTitle());
			}
	}

	public static void main(String[] args) {
		// Create two libraries
		Library firstLibrary = new Library("10 Main St.");
		Library secondLibrary = new Library("228 Liberty St.");

		// Add four books to the first library
		firstLibrary.addBook(new Book("The Da Vinci Code"));
		firstLibrary.addBook(new Book("Le Petit Prince"));
		firstLibrary.addBook(new Book("A Tale of Two Cities"));
		firstLibrary.addBook(new Book("The Lord of the Rings"));

		// Print opening hours and the addresses
		System.out.println("Library hours:");
		printOpeningHours();
		System.out.println();

		System.out.println("Library addresses:");
		firstLibrary.printAddress();
		secondLibrary.printAddress();
		System.out.println();

		// Try to borrow The Lords of the Rings from both libraries
		System.out.println("Borrowing The Lord of the Rings:");
		firstLibrary.borrowBook("The Lord of the Rings");
		firstLibrary.borrowBook("The Lord of the Rings");
		secondLibrary.borrowBook("The Lord of the Rings");
		System.out.println();

		// Print the titles of all available books from both libraries
		System.out.println("Books available in the first library:");
		firstLibrary.printAvailableBooks();
		System.out.println();
		System.out.println("Books available in the second library:");
		secondLibrary.printAvailableBooks();
		System.out.println();

		// Return The Lords of the Rings to the first library
		System.out.println("Returning The Lord of the Rings:");
		firstLibrary.returnBook("The Lord of the Rings");
		System.out.println();

		// Print the titles of available from the first library
		System.out.println("Books available in the first library:");
		firstLibrary.printAvailableBooks();
	}
}